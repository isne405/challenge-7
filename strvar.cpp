
//strvar.cpp

#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
//Uses cstddef and cstdlib
StringVar::StringVar(int size) : max_length(size){
	value = new char[max_length + 1];
	value[0] = '\0';
}

//Uses cstddef and cstdlib
StringVar::StringVar() : max_length(100){
	value = new char[max_length + 1];
	value[0] = '\0';
}

// Uses cstring, cstddef and cstdlib
StringVar::StringVar(const char a[]) : max_length(strlen(a)){
	value = new char[max_length + 1];

	for(int i=0; i<strlen(a); i++){
		value[i] = a[i];
	}
	value[strlen(a)]='\0';
}

//Uses cstring, cstddef and cstdlib
StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length()){
	value = new char[max_length + 1];
	for(int i=0; i<strlen(string_object.value); i++){
		value[i] = string_object.value[i];
	}
	value[strlen(string_object.value)]='\0';
}

StringVar::~StringVar(){
	delete [] value;
}

//Uses cstring
int StringVar::length() const{
	return strlen(value);
}

//Uses iostream
void StringVar::input_line(istream& ins){
	ins.getline(value, max_length + 1);
}

//Uses iostream
ostream& operator << (ostream& outs, const StringVar& the_string){
	outs << the_string.value;
	return outs;
}

/*istream& operator >>(istream& inp , StringVar tmp_str){
	for(int i = 0 ; i < tmp_str.max_length ;i++){
		inp >> tmp_str.value[i];
	}

	return inp;
}*/

bool operator ==(StringVar S1 , StringVar S2){
	int count = 0;
	while(S1.value[count] != '/0'){
		if(S1.value[count] != S2.value[count]){
			return false;	
		}
	count++;
	}
	if(S2.value[count] != '/0'){
		return false;
	}else{
		return true;
	}
}

StringVar operator+(StringVar str1, StringVar str2){
	StringVar str3;
	int counter = 0;
	str3.max_length = str1.max_length + str2.max_length + 10;
	while(str1.value[counter] != '/0'){
		str3.value[counter] = str1.value[counter];
		counter++;
	}
	int another_counter = 0;
	while(str2.value[another_counter] != '/0'){
		str3.value[counter] = str2.value[another_counter];
		counter++;
		another_counter++;
	}
	if(str2.value[another_counter] == '/0'){
		str3.value[counter] = str1.value[another_counter];
		return str3;
	}
}

char StringVar::one_char(int pos){
	return value[pos-1];
}

void StringVar::set_char(int pos , char tmp_char){
	value[pos - 1] = tmp_char;
}

StringVar StringVar::copy_piece(int begin , int wanted_amount){
	StringVar tmp;

	for(int i = 0 ; i < wanted_amount ; i++){
		tmp.value[i] = value[i + begin];
		if(wanted_amount - i == 1){
			tmp.value[i+1] = '/0';
		}
	}

	return tmp;


}

}//strvarken